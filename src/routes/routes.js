import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
// import Overview from 'src/components/Dashboard/Views/Overview.vue'
import PatientInfo from 'src/components/Dashboard/Views/PatientInfo/PatientInfo.vue'
import Patients from 'src/components/Dashboard/Views/Patients/Patients.vue'
import Notifications from 'src/components/Dashboard/Views/Notifications/Notifications.vue'
import Doctors from 'src/components/Dashboard/Views/Doctors/Doctors.vue'
import Devices from 'src/components/Dashboard/Views/Devices/Devices.vue'

const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/admin/notifications'
  },
  {
    path: '/admin',
    component: DashboardLayout,
    redirect: '/admin/patient',
    children: [
      {
        path: 'notifications',
        name: 'Notificaciones',
        component: Notifications
      },
      {
        path: 'patients',
        name: 'Pacientes',
        component: Patients
      },
      {
        path: 'patient/:id',
        name: 'Informacion del Paciente',
        component: PatientInfo
      },
      {
        path: 'patient/:id/:alertId',
        name: 'Informacion del Paciente',
        component: PatientInfo
      },
      {
        path: 'doctors',
        name: 'Doctores',
        component: Doctors
      },
      {
        path: 'devices',
        name: 'Dispositivos',
        component: Devices
      }
    ]
  },
  { path: '*', component: NotFound }
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
