import Sidebar from './SideBar.vue'

const SidebarStore = {
  showSidebar: false,
  sidebarLinks: [
    {
      name: 'Notificaciones',
      icon: 'ti-bell',
      path: '/admin/notifications'
    },
    {
      name: 'Pacientes',
      icon: 'ti-user',
      path: '/admin/patients'
    },
    {
      name: 'Doctores',
      icon: 'ti-heart',
      path: '/admin/doctors'
    },
    {
      name: 'Dispositivos',
      icon: 'ti-tablet',
      path: '/admin/devices'
    }
  ],
  displaySidebar (value) {
    this.showSidebar = value
  }
}

const SidebarPlugin = {

  install (Vue) {
    Vue.mixin({
      data () {
        return {
          sidebarStore: SidebarStore
        }
      }
    })

    Object.defineProperty(Vue.prototype, '$sidebar', {
      get () {
        return this.$root.sidebarStore
      }
    })
    Vue.component('side-bar', Sidebar)
  }
}

export default SidebarPlugin
