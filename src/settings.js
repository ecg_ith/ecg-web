const settings = {
  // baseUrl: 'http://10.19.21.36:8080/api',
  // socketUrl: 'http://10.19.21.36:8080',
  baseUrl: 'http://localhost:8080/api',
  socketUrl: 'ws://localhost:8080'
};

export default settings;
