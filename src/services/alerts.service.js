import axios from 'axios'
import settings from '../settings'

let service = {
  getNotifications: function () {
    return axios.get(settings.baseUrl + '/alerts');
  },
  getAlert: function (alertId) {
    return axios.get(settings.baseUrl + '/alerts/' + alertId);
  }
}
export default service;
