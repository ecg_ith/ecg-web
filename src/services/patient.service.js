import axios from 'axios'
import settings from '../settings'

let service = {
  getAllPatients: function () {
    return axios.get(settings.baseUrl + '/patients')
  },
  createPatient: function (patient) {
    return axios.post(settings.baseUrl + '/patients', patient)
  },
  removePatient: function (patientId) {
    return axios.delete(settings.baseUrl + '/patients/' + patientId)
  },
  editPatient: function (patient) {
    return axios.post(settings.baseUrl + '/patients/' + patient._id, patient)
  },
  getPatient: function (patientId) {
    return axios.get(settings.baseUrl + '/patients/' + patientId)
  },
  getAlertsOfThePatient: function (patientId) {
    return axios.get(settings.baseUrl + '/patients/' + patientId + '/alerts')
  }
}
export default service
