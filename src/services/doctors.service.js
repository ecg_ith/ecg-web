import axios from 'axios'
import settings from '../settings'

let service = {
  getAllDoctors: function () {
    return axios.get(settings.baseUrl + '/doctors')
  },
  createDoctor: function (doctor) {
    return axios.post(settings.baseUrl + '/doctors', doctor)
  },
  removeDoctor: function (doctorId) {
    return axios.delete(settings.baseUrl + '/doctors/' + doctorId)
  },
  editDoctor: function (doctor) {
    return axios.post(settings.baseUrl + '/doctors/' + doctor._id, doctor)
  },
  getDoctor: function (doctorId) {
    return axios.get(settings.baseUrl + '/doctors/' + doctorId)
  }
}
export default service
